#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include "line.h"


using namespace std;
namespace Ui {
class DrawingWindow;
};

class DrawingWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn DrawingWindow::DrawingWindow(QWidget *parent)
    /// \~English
    /// \brief Constructor.
    /// \param parent  when creating a DrawingWindow in the main function, leave this parameter empty
    /// \~Spanish
    /// \brief Constructor
    /// \param parent (ventana padre) Dejar el parametro de parent vacio cuando se cree el DrawingWindow en la funcion de main.
    explicit DrawingWindow(QWidget *parent = 0);

    /// \fn DrawingWindow::~DrawingWindow - the destructor
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~DrawingWindow();

    /// \fn void DrawingWindow::addLine(int x0, int y0, int x1, int y1, int width, QColor color)
    /// \~English
    /// \brief Add a line to the window, specifying coordinates
    /// of the starting and end points.
    /// \param x0  starting x
    /// \param y0  starting y
    /// \param x1  end x
    /// \param y1  end y
    /// \param width - line width
    /// \param color - line color
    /// \~Spanish
    /// \brief Anade una linea a la ventana, especificando las coordenadas
    /// de los puntos inciales y finales.
    /// \param x0  x inicial
    /// \param y0  y inicial
    /// \param x1  x final
    /// \param y1  y final
    /// \param width  ancho de la linea
    /// \param color  color de la linea
    void addLine(int x0, int y0, int x1, int y1, int width, QColor color);

    /// \fn void DrawingWindow::addLinePolar(int x0, int y0, int length, double angle, int width, QColor color)
    /// \~English
    /// \brief Add a line to the window, specifying coordinates
    /// of the starting point, the length and angle.
    /// \param x0  starting x
    /// \param y0  starting y
    /// \param length  length of the line
    /// \param angle  angle
    /// \param width  line width
    /// \param color  line color
    /// \~Spanish
    /// \brief Anade una linea a la ventana, especificando las coordenadas
    /// del punto inicial, el largo y el angulo.
    /// \param x0  x incial
    /// \param y0  y inicial
    /// \param length  largo de la linea
    /// \param angle  angulo de la linea
    /// \param width  ancho de la linea
    /// \param color  color de la linea
    void addLinePolar(int x0, int y0, int length, double angle, int width, QColor color);

    /// \fn void DrawingWindow::box(int x, int y, int sideLength, QColor c)
    /// \~English
    /// \brief Draws a box with sides of size sideLength
    /// \param x initial x coordinate of the box
    /// \param y initial y coordinate of the box
    /// \param sideLength length of the sides of the box
    /// \param c color of the box
    /// \~Spanish
    /// \brief Dibuja una caja con los lados del tamano sideLength
    /// \param x coordenada inicial x de la caja
    /// \param y coordenada inicial y de la caja
    /// \param sideLength largo de los lados de la caja
    /// \param c color de la caja
    void box(int x, int y, int sideLength, QColor c) ;

    /// \fn void DrawingWindow::boxes(int x, int y, int sideLength, double shrinkFactor, int smallestLength, QColor c)
    /// \~English
    /// \brief Recursive function that draws smaller boxes inside the four
    /// corners of the boxes.
    /// \param x initial coordinate x
    /// \param y initial coordinate y
    /// \param sideLength length of the sides of the box
    /// \param shrinkFactor factor to decrease the sideLength in
    /// the recursion for the interior boxes
    /// \param smallestLength smallest length of the size of the
    /// side of the boxes
    /// \param c color of the boxes
    /// \~Spanish
    /// \brief Funcion recursiva que dibuja cajas mas pequenas dentro de las cuatro
    /// esquinas de las cajas.
    /// \param x coordenada inicial x
    /// \param y coordenada inicial y
    /// \param sideLength largo de los lados de la caja
    /// \param shrinkFactor factor para disminuir el tamano de los lados en
    /// la recursion para las cajas interiores
    /// \param smallestLength largo mas pequeno del tamano de el
    /// lado de las cajas
    /// \param c color de las cajas
    void boxes(int x, int y, int sideLength, double shrinkFactor, int smallestLength, QColor c) ;

    /// \fn void DrawingWindow::snowHelper(int size, int level)
    /// \~English
    /// \brief interface function for the snowflake recursive function
    /// \param size snowflake size
    /// \param level recursion depth
    /// \~Spanish
    /// \brief funcion de interface para la funcion recursiva snowflake
    /// \param size tamano del copo de nieve
    /// \param level profundidad de la recursion
    void snowHelper(int size, int level) ;

    /// \fn void DrawingWindow::snowflake(int x, int y, int size, double angle, int level, QColor c) {
    /// \~English
    /// \brief Recursive figure, the snowflake presented at:
    /// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion
    /// sf = sf(n-1) + 60 degrees sf(n-1) + 60 degrees sf(n-1) + s(n-1)
    /// sf(0) = line
    /// \param x initial coordinate x
    /// \param y initial coordinate y
    /// \param size size of the snowflake
    /// \param angle angle of a line of the snowflake
    /// \param level recursion depth
    /// \param c color of the lines (snowflake)
    /// \~Spanish
    /// \brief Figura recursiva, el copo de nieve presentado en:
    /// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion
    /// sf = sf(n-1) + 60 grados sf(n-1) + 60 grados sf(n-1) + s(n-1)
    /// sf(0) = line
    /// \param x coordenada inicial x
    /// \param y coordenada inicial y
    /// \param size tamano del copo de nieve
    /// \param angle angulo de la linea del copo de nieve
    /// \param level profundidad de la recursion
    /// \param c color de las lineas (copo de nieve)
    void snowflake(int x, int y, int size, double angle, int level, QColor c) ;
private:
    Ui::DrawingWindow *ui;
    vector <Line *> *vL;

protected:

    /// \fn void DrawingWindow::paintEvent(QPaintEvent *)
    /// \~English
    /// \brief Function called in a paint event
    /// \~Spanish
    /// \brief Funcion invocada en un evento de pintar
    void paintEvent(QPaintEvent *);
};

#endif // MAINWINDOW_H
