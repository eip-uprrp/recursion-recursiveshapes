#include "drawingWindow.h"

/// \fn void DrawingWindow::box(int x, int y, int sideLength, QColor c)
/// \~English
/// \brief Draws a box with sides of size sideLength
/// \param x initial x coordinate of the box
/// \param y initial y coordinate of the box
/// \param sideLength length of the sides of the box
/// \param c color of the box
/// \~Spanish
/// \brief Dibuja una caja con los lados del tamano sideLength
/// \param x coordenada inicial x de la caja
/// \param y coordenada inicial y de la caja
/// \param sideLength largo de los lados de la caja
/// \param c color de la caja
void DrawingWindow::box(int x, int y, int sideLength, QColor c) {
    addLine(x,y,x+sideLength,y,1,c);
    addLine(x+sideLength,y,x+sideLength,y+sideLength,1,c);
    addLine(x+sideLength,y+sideLength,x,y+sideLength,1,c);
    addLine(x,y+sideLength,x,y,1,c);
}



/// \fn void DrawingWindow::boxes(int x, int y, int sideLength, double shrinkFactor, int smallestLength, QColor c)
/// \~English
/// \brief Recursive function that draws smaller boxes inside the four
/// corners of the boxes.
/// \param x initial coordinate x
/// \param y initial coordinate y
/// \param sideLength length of the sides of the box
/// \param shrinkFactor factor to decreese the sideLength in
/// the recursion for the interior boxes
/// \param smallestLength smallest length of the size of the
/// side of the boxes
/// \param c color of the boxes
/// \~Spanish
/// \brief Funcion recursiva que dibuja cajas mas pequenas dentro de las cuatro
/// esquenas de las cajas.
/// \param x coordenada inicial x
/// \param y coordenada inicial y
/// \param sideLength largo de los lados de la caja
/// \param shrinkFactor factor para disminuir el tamano de los lados en
/// la recursion para las cajas interiores
/// \param smallestLength largo mas pequeno del tamano del
/// lado de las cajas
/// \param c color de las cajas
void DrawingWindow::boxes(int x, int y, int sideLength, double shrinkFactor, int smallestLength, QColor c) {

    // YOUR CODE HERE
   
}
