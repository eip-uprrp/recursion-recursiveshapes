#include "drawingWindow.h"
#include <cmath>


/// \fn void DrawingWindow::snowflake(int x, int y, int size, double angle, int level, QColor c) {
/// \~English
/// \brief Recursive figure, the snowflake presented at:
/// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion
/// sf = sf(n-1) + 60 degrees sf(n-1) + 60 degrees sf(n-1) + s(n-1)
/// sf(0) = line
/// \param x initial coordinate x
/// \param y initial coordinate y
/// \param size size of the snowflake
/// \param angle angle of a line of the snowflake
/// \param level recursion depth
/// \param c color of the lines (snowflake)
/// \~Spanish
/// \brief Figura recursiva, el copo de nieve presentado en:
/// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion
/// sf = sf(n-1) + 60 grados sf(n-1) + 60 grados sf(n-1) + s(n-1)
/// sf(0) = line
/// \param x coordenada inicial x
/// \param y coordenada inicial y
/// \param size tamano del copo de nieve
/// \param angle angulo de la linea del copo de nieve
/// \param level profundidad de la recursion
/// \param c color de las lineas (copo de nieve)
void DrawingWindow::snowflake(int x, int y, int size, double angle, int level, QColor c) {
    int chunkSize = round(size/3);

    if (level == 0) {
        addLinePolar(x,y,size,angle,1,c);
        return;
    }


    snowflake(x, y, chunkSize, angle, level-1, c);

    x = x + round( chunkSize * cos(angle * M_PI/180));
    y = y + round( chunkSize * sin(angle * M_PI/180));
    snowflake(x, y, chunkSize, angle + 60, level-1, c);

    snowflake(x + round(chunkSize * cos((angle + 60) * M_PI/180)),
                  y + round(chunkSize * sin((angle + 60) * M_PI/180)),
                  chunkSize, angle - 60, level-1, c);

    x = x + round(chunkSize * cos(angle * M_PI/180));
    y = y + round(chunkSize * sin(angle * M_PI/180));

    snowflake(x, y, chunkSize, angle, level-1, c);
}


/// \fn void DrawingWindow::snowHelper(int size, int level)
/// \~English
/// \brief interface function for the snowflake recursive function
/// \param size snowflake size
/// \param level recursion depth
/// \~Spanish
/// \brief funcion de interface para la funcion recursiva snowflake
/// \param size tamano del copo de nieve
/// \param level profundidad de la recursion
void DrawingWindow::snowHelper(int size, int level) {

    snowflake(50,size,size,0,level,Qt::blue);
    snowflake(round(size*cos(-60*M_PI/180))+50,size+round(size*sin(-60*M_PI/180)),size,120,level,Qt::blue);
    snowflake(size+50,size,size,-120,level,Qt::blue);
}
